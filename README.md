# Docker image for dart testing

This docker image contains all the necessary tools to do dart testing.

This include :
 - dart
 - chrome
 - firefox
 - lcov
 - dart package coverage installed globally
 - firebase
 
 ## Update
 
 This image will be update one per week